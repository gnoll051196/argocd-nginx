FROM nginx:latest

# Копируем кастомную стартовую страницу в директорию по умолч
COPY index.html /usr/share/nginx/html/index.html

# Копируем кастомную картинку в директорию по умолч
COPY image.jpg /usr/share/nginx/html/image.jpg